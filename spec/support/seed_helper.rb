require 'fileutils'

module SeedHelper
  GITLAB_URL = "https://gitlab.com/gitlab-org/gitlab-elasticsearch-git-test.git"

  def ensure_seeds
    if File.exists?(SUPPORT_PATH)
      FileUtils.rm_r(SUPPORT_PATH)
    end

    FileUtils.mkdir_p(SUPPORT_PATH)

    create_test_bare_repo
  end

  def create_test_bare_repo
    system(git_env, *%W(git clone --bare #{GITLAB_URL}), chdir: SUPPORT_PATH)
  end

  # Prevent developer git configurations from being persisted to test
  # repositories
  def git_env
    {'GIT_TEMPLATE_DIR' => ''}
  end
end
