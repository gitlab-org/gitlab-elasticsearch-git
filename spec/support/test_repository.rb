class TestRepository
  include Elasticsearch::Git::Repository

  CLIENT = Elasticsearch::Client.new(
    host: (ENV['ELASTICSEARCH_HOST'] || 'localhost')
  )

  self.__elasticsearch__.client = CLIENT

  def initialize(id = 'test_1', project_id = 1)
    @repo_id = id
    @project_id = 1
  end

  def client_for_indexing
    CLIENT
  end

  def repository_id
    @repo_id
  end

  def project_id
    @project_id
  end

  def path_to_repo
    TEST_REPO_PATH
  end
end
