# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'elasticsearch/git/version'

Gem::Specification.new do |spec|
  spec.name          = "gitlab-elasticsearch-git"
  spec.version       = Elasticsearch::Git::VERSION
  spec.authors       = ["Andrey Kumanyaev", "Evgeniy Sokovikov", "GitLab B.V."]
  spec.email         = ["me@zzet.org", "skv-headless@yandex.ru"]
  spec.summary       = %q{Elasticsearch integrations for git repositories.}
  spec.description   = %q{Elasticsearch integrations for indexing git repositories.}
  spec.homepage      = "https://gitlab.com/gitlab-org/gitlab-elasticsearch-git"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'elasticsearch-model', '~> 0.1.9'
  spec.add_runtime_dependency 'elasticsearch-api'
  spec.add_runtime_dependency 'rugged', '~> 0.24'
  spec.add_runtime_dependency 'charlock_holmes', '~> 0.7'
  spec.add_runtime_dependency 'github-linguist', '~> 4.7'
  spec.add_runtime_dependency 'activemodel', '~> 4.2'
  spec.add_runtime_dependency 'activesupport', '~> 4.2'
end
